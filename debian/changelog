fonts-evertype-conakry (0.002+source-5) unstable; urgency=medium

  * debian/control
    - Use secure URI in Homepage field.
    - Drop unnable to satisfy Suggests: field
    - Set Standards-Version: 4.5.1

 -- Hideki Yamane <henrich@debian.org>  Thu, 10 Jun 2021 00:05:56 +0900

fonts-evertype-conakry (0.002+source-4) unstable; urgency=medium

  * debian/control
    - Update Maintainer address
    - Remove Christian Perrier from Uploaders (Closes: #927644)
    - Use dh13
    - Set Standards-Version: 4.5.0
    - Update Vcs-* to point salsa.debian.org
    - Add Rules-Requires-Root: no
    - Drop obsolete ttf- dependencies
  * debian/rules
    - Use default compression
  * debian/install
    - Fix installation to /usr/share/fonts/truetype/evertype-conakry
  * Add debian/salsa-ci.yml

 -- Hideki Yamane <henrich@debian.org>  Sun, 02 Aug 2020 19:25:26 +0900

fonts-evertype-conakry (0.002+source-3) unstable; urgency=low

  * Update Standards to 3.9.4 (checked)
  * Bump debhelper compatibility to 9
  * Drop transitional package
  * Use Breaks instead of Conflicts. Drop Provides as it is no
    longer needed (installations should have transitioned since wheezy
    and the package has anyway no reverse dependency.
  * Use xz extreme compression for deb packages
  * Use git for packaging: adapt Vcs-* fields
  * Add Multi-Arch: foreign field
  * Add explicit Copyright mention to debian/copyright

 -- Christian Perrier <bubulle@debian.org>  Fri, 23 Aug 2013 09:31:55 +0200

fonts-evertype-conakry (0.002+source-2) unstable; urgency=low

  * Add proper Replaces, Provides, Conflicts to handle smooth upgrades
    after renaming

 -- Christian Perrier <bubulle@debian.org>  Mon, 17 Oct 2011 21:32:38 +0200

fonts-evertype-conakry (0.002+source-1) unstable; urgency=low

  [ Paul Wise ]
  * Clean the package description of unneeded information (Closes: #563978)

  [ Christian Perrier ]
  * Team upload
  * Drop x-ttcidfont-conf, fontconfig et al. from Suggests
  * Rename source package to "fonts-evertype-conakry" to fit the Font
    Packages Naming Policy.
  * Bump Standards to 3.9.2 (checked)
  * Change fonts install directory from
    usr/share/fonts/truetype/ttf-evertype-conakry to
    usr/share/fonts/truetype/evertype-conakry

  [ Nicolas Spalinger ]
  * Added fonts source to "upstream" tarball, so new upstream version

 -- Christian Perrier <bubulle@debian.org>  Fri, 26 Aug 2011 23:01:09 +0200

ttf-evertype-conakry (0.002-1) unstable; urgency=low

  [ Nicolas Spalinger ]
  * Initial release under OFL (Closes: #453882)

 -- Christian Perrier <bubulle@debian.org>  Sun, 13 Dec 2009 18:57:54 +0100
